
public class Fizz {
//req 1: green - minimum code to pass test case 
	public String getFizzBuzz(int i) {
		if(i<0) {
			return "Error!";	
		}
		if(i%15==0) {
			return "FizzBuzz";
		}
		if(i%5==0)
		{
			
			return "Buzz";
		}
		else if(i%3==0)
		{
			
			return "Fizz";
		}
		return "1";
		
	}
}
