import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


//stub getFizzBuzz(number)
class TestFizzBuzz {

	//Requirement 1
	@Test
	void testInputGreaterThanZero() {
		//Req 1 - red case: getFizzBuzz not yet implemented
		Fizz fb = new Fizz();
		String result = fb.getFizzBuzz(1);
		assertEquals("1", result);
	}
	
	@Test
	void testInputLessThanZero() {
		Fizz fb = new Fizz();
		String result = fb.getFizzBuzz(-5);
		assertEquals("Error!", result);
	}
	
	@Test
	//Requirement 3
	void testInputIsDivisibleBy5() {
		Fizz fb = new Fizz();
		String result = fb.getFizzBuzz(25);
		assertEquals("Buzz", result);
		
	}
	@Test
	//Requirement 2
	void testInputIsDivisibleBy3() {
		Fizz fb = new Fizz();
		String result = fb.getFizzBuzz(25);
		assertEquals("Fizz", result);
		
	}
	@Test
	//Requirement 4
	void testInputIsDivisibleBy15() {
		Fizz fb = new Fizz();
		String result = fb.getFizzBuzz(30);
		assertEquals("FizzBuzz", result);
		
	}

}
